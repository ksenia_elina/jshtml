<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="utf-8">
        <script src="https://code.jquery.com/jquery-3.5.0.js"></script>
        <script type="text/javascript" src="jquery.serializejson.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>       
        <title>Тег SELECT</title>
    </head>
    <body>  
        
        <form action="" method="get">
            <p><select size="3" multiple name="hero1[]">
                <option disabled>Выберите героя</option>
                <option value="1">1</option>
                <option selected value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="4">5</option>
            </select></p>
            
            <p><select size="3" multiple name="hero2[]">
                <option disabled>Выберите героя</option>
                <option value="1">1</option>
                <option selected value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="4">5</option>
            </select></p>
            
            <p><select size="3" multiple name="hero3[]">
                <option disabled>Выберите героя</option>
                <option value="1">1</option>
                <option selected value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="4">5</option>
            </select></p>
            
            <p><select size="3" multiple name="hero4[]">
                <option disabled>Выберите героя</option>
                <option value="1">1</option>
                <option selected value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="4">5</option>
            </select></p>
            
            <p><select size="3" multiple name="hero5[]">
                <option disabled>Выберите героя</option>
                <option value="1">1</option>
                <option selected value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="4">5</option>
            </select></p>
            
            <input type="text" name="n1"></br>
            <input type="text" name="n2"></br>
            <button type="submit" class="a_submit">Submit</button>
            
        </form>
        <p></p><div class="wrapper"></div></p>

                <script>
            $('.a_submit').on('click', function (form) {
                form.preventDefault();
                console.log($('form').serializeJSON());
                $('.wrapper').html(JSON.stringify($('form').serializeJSON()));
                
                $.ajax({ 
                type: "GET",
                url: "/test.php",
               data: JSON.stringify($('form').serializeJSON()),
               success: function(){
                    alert( "Data Saved: " );
              }});
            });
            
        </script> 
        
    </body>
</html>