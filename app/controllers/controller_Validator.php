<?php
class Controller_Validator extends Controller
{

	function __construct()
	{
		$this->model = new Model_Validator();
		$this->view = new View();
	}
	//загрузка автоматическая при заходе на сайт
	function action_index()
	{
        

       $res = $this->model->get_data($csv);
		$this->view->generate('Validator_view.php', 'template_view.php', $res);
	}
    //ручная загрузка
    function action_add()
	{
        //tecdate
        $csv = array_map('str_getcsv', file("MOCK_DATA.csv"));
        array_walk($csv, function(&$a) use ($csv) {
          $a = array_combine($csv[0], $a);
        });
        array_shift($csv); # remove column header
		$this->model->add_data($csv);	

	}
    
    protected function ValidateProperties()
    {
        
    }


}
?>